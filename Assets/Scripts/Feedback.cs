﻿using UnityEngine;
using System.Collections;

public class Feedback : MonoBehaviour
{
    private bool canChangeColor = true;


    public IEnumerator DamageRecive()
    {
        if (canChangeColor)
        {
            canChangeColor = false;
            double x = 0f;
            Color defaultColor = GetComponent<Renderer>().material.color;
            float maxValue = defaultColor.r;

            while (x < maxValue*0.95)
            {
                x += (maxValue - x)*0.1f;
                //Debug.Log(x);
                Color fadeColor = GetComponent<Renderer>().material.color;
                fadeColor.g = (float) x;
                fadeColor.b = (float) x;
                GetComponent<Renderer>().material.color = fadeColor;

                yield return null;
            }

            GetComponent<Renderer>().material.color = defaultColor;
            canChangeColor = true;
        }
    }

    public IEnumerator ScaleAndDestroy(float startSize, float endSize, float delayBeforeStart, float delayDestroy)
    {
        yield return new WaitForSeconds(delayBeforeStart);

        float x = startSize;

        if (startSize > endSize)
        {
            while (x > endSize*0.95f)
            {
                x -= (endSize + x)*0.1f;
                transform.localScale = new Vector3(x, x, x);

                yield return null;
            }
        }
        else
        {
            while (x < endSize*0.95f)
            {
                x += (endSize - x)*0.1f;
                transform.localScale = new Vector3(x, x, x);

                yield return null;
            }
        }
        if (delayDestroy >= 0)
        {
            yield return new WaitForSeconds(delayDestroy);
            Destroy(gameObject);
        }
    }
}