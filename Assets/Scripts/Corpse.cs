﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;


public class Corpse : MonoBehaviour
{
    public bool isAllChilden;
    public float selfDestruct;

    private void Awake()
    {
        Invoke("SelfDestruct", selfDestruct);

        if (isAllChilden)
        {
            foreach (Transform child in transform)
            {
                //child.gameObject.AddComponent<ColorFeedback>();
                Feedback childColorFade = child.gameObject.GetComponent<Feedback>();
                childColorFade.StartCoroutine(childColorFade.DamageRecive());
            }
        }
        else
        {
            Feedback selfColorFade = gameObject.GetComponent<Feedback>();
            selfColorFade.StartCoroutine(selfColorFade.DamageRecive());
        }
    }


    public void SelfDestruct()
    {
        if (isAllChilden)
        {
            foreach (Transform child in transform)
            {
                Feedback childDestruct = child.gameObject.GetComponent<Feedback>();
                childDestruct.StartCoroutine(childDestruct.ScaleAndDestroy(1, 0, Random.Range(0f, 2f), 0.1f));
            }
        }
        else
        {
            Feedback selfDestruct = gameObject.GetComponent<Feedback>();
            selfDestruct.StartCoroutine(selfDestruct.ScaleAndDestroy(1, 0, Random.Range(0f, 2f), 0.1f));
        }
    }
}