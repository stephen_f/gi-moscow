﻿using UnityEngine;
using System.Collections;

public class Dropbox : MonoBehaviour
{
    public GameObject[] botPrefab;
    public Transform spawnPoints;
    private HingeJoint[] joints;
    public Rigidbody floor;
    public float waitBeforeDestroy = 10f;
    public ArrayList spawnedBots = new ArrayList();
    private bool selfDestruct = false;
    private bool moveDestruct = false;
    private bool invincible = true;


    public void Start()
    {
        Invoke("Inbvinsible", 2f);

        joints = GetComponentsInChildren<HingeJoint>();
        floor.velocity.Set(0, -1, 0);

        //Ищем все все точки спавна
        foreach (Transform child in spawnPoints.transform)
        {
            spawnedBots.Add(Instantiate(botPrefab[Random.Range(0, botPrefab.Length)], child.position, child.rotation));
        }
    }

    public void Update()
    {
        if (!selfDestruct)
        {
            if (!invincible)
            {
                if (floor.velocity.magnitude <= 0.1f)
                {
                    foreach (HingeJoint x in joints)
                    {
                        JointLimits limitsNew = x.limits;
                        limitsNew.min = 10;
                        limitsNew.max = 90;
                        x.limits = limitsNew;
                    }
                    Invoke("SetKinematic", 2f);
                    selfDestruct = true;
                }
            }
        }

        if (moveDestruct)
        {
            transform.position = new Vector3(
                transform.position.x,
                Mathf.Lerp(transform.position.y, transform.position.y - 1, 2f*Time.deltaTime),
                transform.position.z
                );
        }
    }

    public void Inbvinsible()
    {
        invincible = false;
    }

    //Выключаем физику
    public void SetKinematic()
    {
        floor.isKinematic = true;
        foreach (HingeJoint x in joints)
        {
            x.GetComponent<Rigidbody>().isKinematic = true;
        }


        foreach (GameObject i in spawnedBots)
        {
            if (i != null)
                i.GetComponent<BotAI>().aiEnable = true;
        }
        Invoke("SelfDestruct", waitBeforeDestroy);
    }


    public void SelfDestruct()
    {
        moveDestruct = true;
        Destroy(gameObject, waitBeforeDestroy);
    }
}