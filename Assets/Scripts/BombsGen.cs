﻿using UnityEngine;
using System.Collections;

public class BombsGen : MonoBehaviour
{
    public Vector2 maxPoint;
    public Vector2 minPoint;
    public float height;
    public float spawnRate = 1.0f;
    public Transform[] bombPrefab;


    public void Start()
    {
        InvokeRepeating("BombSpawn", spawnRate, spawnRate);
    }

    public void Update()
    {
    }

    public void BombSpawn()
    {
        Instantiate(bombPrefab[Random.Range(0, bombPrefab.Length)], new Vector3(
            Random.Range(minPoint.x, maxPoint.x),
            height,
            Random.Range(minPoint.y, maxPoint.y)), Quaternion.identity);
    }
}