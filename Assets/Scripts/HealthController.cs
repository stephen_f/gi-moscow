﻿using UnityEngine;
using System.Collections;
using System.ComponentModel;

public class HealthController : MonoBehaviour
{
    public float hp = 100;
    public GameObject corpse;


    public void OnDamage(float reciveDamage)
    {
        hp -= reciveDamage;

        Feedback colorFade = gameObject.GetComponent<Feedback>();
        colorFade.StartCoroutine(colorFade.DamageRecive());


        if (hp <= 0)
            OnDeath();
    }


    public void OnDeath()
    {
        if (corpse != null)
            Instantiate(corpse, transform.position, transform.rotation);
        Destroy(gameObject);
    }
}