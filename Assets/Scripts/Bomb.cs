﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour
{
    public bool isThroughColliders = false;
    public float radius = 10f;
    public float force = 10f;
    public float maxDamage = 100f;
    public float minDamage = 50f;

    private float damage = 0f;
    private Vector3 lastPos;

    public void Start()
    {
        damage = Random.Range(minDamage, maxDamage);
    }


    public void OnCollisionEnter(Collision collision)
    {
        gameObject.GetComponent<Collider>().enabled = false;

        Collider[] colliders = Physics.OverlapSphere(collision.contacts[0].point, radius);

        foreach (Collider i in colliders)
        {
            if (i.GetComponent<Rigidbody>() == null) continue;
            lastPos = i.transform.position;

            if (isThroughColliders)
            {
                DoDamage(i.gameObject);
            }
            else
            {
                //Пускает луч и проверяет если перед целью коллайдеры
                RaycastHit hit;
                Ray targetRay = new Ray(transform.position, i.transform.position - transform.position);


                if (Physics.Raycast(targetRay, out hit, radius))
                {
                    if (hit.collider.CompareTag("Wall"))
                    {
                        //Debug.Log("Не вижу");
                        break;
                    }
                }

                DoDamage(i.gameObject);
            }
        }

        Color transparentSet = GetComponent<Renderer>().material.color;
        transparentSet.a = 0.1f;
        GetComponent<Renderer>().material.color = transparentSet;
        GetComponent<Collider>().enabled = false;

        Feedback selfColorFadeAndDestuct = gameObject.GetComponent<Feedback>();
        selfColorFadeAndDestuct.StartCoroutine(selfColorFadeAndDestuct.DamageRecive());
        selfColorFadeAndDestuct.StartCoroutine(selfColorFadeAndDestuct.ScaleAndDestroy(1, radius, 0f, -1));
        selfColorFadeAndDestuct.StartCoroutine(selfColorFadeAndDestuct.ScaleAndDestroy(radius, 0, 0.4f, 0.5f));
    }

    public void DoDamage(GameObject target)
    {
        Debug.DrawRay(transform.position, lastPos - transform.position, Color.red, 1f);

        target.GetComponent<Rigidbody>().AddExplosionForce(force, transform.position, radius, 0f, ForceMode.Impulse);
        target.SendMessage("OnDamage", damage, SendMessageOptions.DontRequireReceiver);
    }
}