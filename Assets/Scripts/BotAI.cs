﻿using System;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;


public class BotAI : MonoBehaviour
{
    public GameObject waypoints;
    private Transform[] points;
    private int destPoint = 0;
    private NavMeshAgent agent;
    private bool _aiEnable = false;

    public bool aiEnable
    {
        get { return _aiEnable; }
        set
        {
            _aiEnable = value;
            GetComponent<NavMeshAgent>().enabled = value;
            GetComponent<Rigidbody>().isKinematic = value;
        }
    }


    public void Awake()
    {
        if (waypoints == null)
            waypoints = GameObject.FindWithTag("Waypoints");
    }


    private void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        points = waypoints.GetComponentsInChildren<Transform>();


        //agent.autoBraking = false;
    }


    private void GotoNextPoint()
    {
        if (points.Length == 0)
            throw new ArgumentNullException("No waypoints");

        destPoint = Random.Range(0, points.Length - 1);
        agent.destination = points[destPoint].position;
    }

    private void Update()
    {
        if (_aiEnable)
            if (agent.remainingDistance < 0.5f)
                GotoNextPoint();
    }
}