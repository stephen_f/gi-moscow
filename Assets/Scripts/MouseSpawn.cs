﻿using UnityEngine;
using System.Collections;

public class MouseSpawn : MonoBehaviour
{
    public Transform[] bombPrefab;
    public float distance = 4.5f;
    private float lastFire = 0f;
    public float offsetY = 5f;
    public float fireRate = 0.5f;

    private void Update()
    {
        if (Input.GetButton("Fire1") && (lastFire + fireRate) < Time.time)
        {
            CastRayToWorld();
        }
    }

    private void CastRayToWorld()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        
        if (Physics.Raycast(ray, out hit, distance))
        {
            Instantiate(bombPrefab[Random.Range(0, bombPrefab.Length)], new Vector3(hit.point.x, hit.point.y + offsetY, hit.point.z), transform.rotation);
        }



        lastFire = Time.time;
    }
}