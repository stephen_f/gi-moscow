﻿using UnityEngine;
using System.Collections;
using System.Security.Cryptography.X509Certificates;

public class DropGen : MonoBehaviour
{
    public GameObject dropPrefab;
    public Transform spawnZone;
    private Vector3 lastSpawn;
    public int botsOnScreen;
    public float OffsetMaxX = 30f;
    public float OffsetZ = 10f;
    public float spawnRate = 5f;


    private void Start()
    {
        lastSpawn = spawnZone.position;
        InvokeRepeating("CountBots", 0f, spawnRate);
    }

    private void CountBots()
    {
        GameObject[] bots;
        bots = GameObject.FindGameObjectsWithTag("Player");

        if (bots.Length <= botsOnScreen)
            SpawnBots();
    }

    private void SpawnBots()
    {
        float offset;

        if ((lastSpawn.x <= spawnZone.position.x + OffsetMaxX) || (lastSpawn.x <= spawnZone.position.x - OffsetMaxX))
            offset = Random.Range(15f, 18f);
        else
        {
            offset = Random.Range(-15f, -18f);
        }


        Vector3 newSpawn = new Vector3(lastSpawn.x + offset, lastSpawn.y,
            spawnZone.position.z + (Random.Range(-OffsetZ, OffsetZ)));

        Instantiate(dropPrefab, newSpawn, Quaternion.identity);
        lastSpawn = newSpawn;
    }
}