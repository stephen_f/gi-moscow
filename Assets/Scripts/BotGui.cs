﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BotGui : MonoBehaviour
{
    private Slider[] sliders;
    private HealthController healthController;

    public void Start()
    {
        sliders = GetComponentsInChildren<Slider>();
        foreach (Slider i in sliders)
        {
            i.maxValue = GetComponent<HealthController>().hp;
        }


        healthController = GetComponent<HealthController>();
    }


    public void Update()
    {
        foreach (Slider hp in sliders)
        {
            hp.value = Mathf.Lerp(hp.value, healthController.hp, 2f*Time.deltaTime);
        }
    }
}